FROM python:3.10.5-slim-buster

ENV PYTHONUNBUFFERED=1

COPY ./requirements.txt /requirements.txt

RUN apt update && apt install -y python3-pip
RUN apt install libpq-dev -y
RUN pip3 install -r /requirements.txt

RUN mkdir /app
COPY ./app/ /app/

WORKDIR /app

